<?xml version="1.0" encoding="UTF-8"?>

<!--
 =================================================================
  Licensed Materials - Property of IBM

  WebSphere Commerce

  (C) Copyright IBM Corp. 2007, 2015 All Rights Reserved.

  US Government Users Restricted Rights - Use, duplication or
  disclosure restricted by GSA ADP Schedule Contract with
  IBM Corp.
 =================================================================
-->

<!--
 ===============================================================================
 This Ant file is the default WCBD build script.
 ===============================================================================
-->

<!--
 ===============================================================================
 CUSTOMIZATION: Make a copy of this file as <project>-build.xml and follow the
 other CUSTOMIZATION comments in this file.
 ===============================================================================
-->
<project name="wcbd-build" default="all">

	<!-- DO NOT change the order of of the following load and import below -->

	<!-- Set up the timestamp for logging -->
	<tstamp>
		<format property="tstamp" pattern="yyyyMMddhhmmss" />
	</tstamp>

	<!-- Load wcbd-version-info.properties to get version info -->
	<property file="${basedir}/wcbd-version-info.properties" />

	<!-- Define WCBD tasks for validation -->
	<path id="init.wcbd.ant.class.path">
		<pathelement location="${basedir}/lib/icu4j.jar" />
		<pathelement location="${basedir}/lib/wcbd-ant.jar" />
		<!--
		 Include the folder that contains the resource bundles for messages
		 printed in the classpath
		-->
		<pathelement location="${basedir}/properties" />
	</path>

	<taskdef name="init.failNL"
	         classname="com.ibm.commerce.wcbd.ant.taskdefs.FailNL"
	         classpathref="init.wcbd.ant.class.path" />
	<taskdef name="init.validateProperty"
	         classname="com.ibm.commerce.wcbd.ant.taskdefs.ValidateProperty"
	         classpathref="init.wcbd.ant.class.path" />

	<!-- set default value for app.type, if the property is already set, this won't change the value -->
	<property name="app.type" value="ts" />
	<!-- set work dir to basedir if it is not specified by caller -->
	<property name="work.dir" value="${basedir}" />
	
	<!--
	 Set properties for loading properties files and creating log dir based on
	 the build.type value
	-->
	<condition property="build.type.suffix" value="-${build.type}-${app.type}" else=""> <!--samsa -->
		<isset property="build.type" />
	</condition>
	<condition property="build.type.prefix" value="${build.type}-${app.type}-" else="">
		<isset property="build.type" />
	</condition>

	<init.failNL bundle="wcbd-build-messages" key="ERR_FILE_NOT_FOUND">
		<arg value="${work.dir}/build${build.type.suffix}.properties" />
		<condition>
			<not>
				<available file="${work.dir}/build${build.type.suffix}.properties"
				           type="file" />
			</not>
		</condition>
	</init.failNL>
	<property file="${work.dir}/build${build.type.suffix}.properties" />

	<!--
	 Validate properties that are required for initialization 
	-->
	<init.validateProperty property="build.label" trim="true" nonempty="true" />
	<init.failNL bundle="wcbd-build-messages" key="ERR_FILE_NOT_FOUND">
		<arg value="${work.dir}/build${build.type.suffix}.private.properties" />
		<condition>
			<not>
				<available file="${work.dir}/build${build.type.suffix}.private.properties"
				           type="file" />
			</not>
		</condition>
	</init.failNL>
	<init.validateProperty property="was.home"
	                       trim="true"
	                       nonempty="true"
	                       fileExist="true" />
	<init.validateProperty property="wc.home"
	                       trim="true"
	                       nonempty="true"
	                       fileExist="true" />
	<init.validateProperty property="db.type">
		<validValue value="db2" />
		<validValue value="oracle" />
		<validValue value="os400" />
	</init.validateProperty>

	<!--
	 ===========================================================================
	 CUSTOMIZATION: Import <project>-build-common.xml instead of
	 wcbd-build-common.xml if available, so targets are added and overridden as
	 per specification of the Ant <import> task.
	 ===========================================================================
	-->
	<!-- Import the WCBD common build Ant file -->
	<import file="${basedir}/wcbd-build-common.xml" />

	<!--
	 Encode, decode and load the required build private properties file
	-->
	<encodeProperties file="${work.dir}/build${build.type.suffix}.private.properties" />
	<decodeLoadProperties file="${work.dir}/build${build.type.suffix}.private.properties" />

	<!--
	 Runs the build process and sends out notifications as appropriate.
	-->
	<target name="all">
		<mkdir dir="${log.dir}" />
		<trycatch property="throwable.msg" reference="throwable">
			<try>
				<ant antfile="${ant.file}"
				     target="build"
				     output="${log.file}" />
				<!-- Send build success e-mail after build tasks are done -->
				<antcall target="mail.success" />
			</try>
			<catch>
				<!-- If build fails, attach log zip file to failure e-mail -->
				<printStackTrace refid="throwable" property="stack.trace" />
				<echo message="${stack.trace}"
				      file="${log.file}"
				      append="true" />
				<delete file="${log.zip.file}" quiet="true" />
				<zip destfile="${log.zip.file}">
					<fileset file="${log.file}" />
				</zip>
				<antcall target="mail.failure">
					<param name="files" value="${log.zip.file}" />
				</antcall>
				<fail message="${throwable.msg}" />
			</catch>
		</trycatch>
	</target>


	<!--
	 Performs the actual build tasks.
	-->
	<target name="build">
		<!--
		 =======================================================================
		 CUSTOMIZATION: Change the logic of the build process as required below
		 by modifying the <antcall> tasks, calling targets in
		 wcbd-build-common.properties and <project>-build-common.properties in
		 the desired sequence.
		 =======================================================================
		-->

		<!-- Extract -->
		<antcall target="extract" />

		<!-- Compile -->
		<antcall target="compile.archive.global" />
		<antcall target="compile.archive.connector" />
		<antcall target="compile.archive.ejb" />
		<antcall target="compile.archive.java" />
		<antcall target="compile.archive.web" />

		<!-- Create server deployment package -->
		<antcall target="package.server" />
		<antcall target="package.server.v9" />

		<!-- Clean working directory -->
		<antcall target="clean.working.dir" />
	</target>

</project>
