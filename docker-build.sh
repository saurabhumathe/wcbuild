#!/bin/bash

set -x #same as set -o xtrace
set -e #same as set -o errexit
set -u #same as set -o nounset



##########################################################################################
# docker-build wrapper for docker-build with WC V9
#
##########################################################################################

umask 0027

SCRIPT=$(readlink -f "$0")
SCRIPTDIR=`dirname "$SCRIPT"`
SCRIPTNAME=`basename $0`

set +u
if [ ! "$1" ] || [ ! "$2" ] ; then
    echo
    echo "You are missing one or more input variables."
    echo "Usage: docker-build.sh <app.type> <build.label>"
    echo
    exit 1
fi
set -u

##########################################################################################
# Main part of the script
##########################################################################################
cd ${SCRIPTDIR}

# Putting Vars into a source file
#source docker-build.env

# Samples for debugging the Build Label
# xDEBUG: BUILD_TYPE="ts-app"
# xDEBUG: BUILD_LABEL="demo-ts-21"
BUILD_TYPE=$1
BUILD_LABEL=$2

##########################################################################################
# Step 0 - login to the docker registry
##########################################################################################
# xDEBUG: echo "${DOCKER_PASSWORD}" | docker login ${DOCKER_REG} -u ${DOCKER_USER} --password-stdin
docker login ${DOCKER_REG} -u ${DOCKER_USER} -p ${DOCKER_PASSWORD}


## Test if Docker Build exist
set +e
docker pull "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_TYPE}:${BUILD_LABEL}" > /dev/null 2>&1
ERROR_CODE=$?
set -e
if [ "$ERROR_CODE" -ne "0" ]; then

	echo "========================================================="
	echo " Building Docker for ${BUILD_TYPE}:${BUILD_LABEL}"
	echo "========================================================="
	echo

	##########################################################################################
	# Step 2 - Unzip file
	##########################################################################################
	rm -fr CusDeploy
	mv wcbd CusDeploy

	##########################################################################################
	# Step 3 - Docker Build - The main work
	##########################################################################################
	docker build -t "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_TYPE}:${BUILD_LABEL}" --build-arg IMAGE_REPO="${DOCKER_REG}" --build-arg IMAGE_TAG="${WC9_VERSION}" .

  # Maybe we can just get by with latest, we don't really need to version it.
	# docker push "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_TYPE}:${BUILD_LABEL}"
	docker tag "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_TYPE}:${BUILD_LABEL}" "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_TYPE}:latest"
	echo "Pushing ${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_TYPE}:latest"
	docker push "${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_TYPE}:latest" > /dev/null 2>&1

	##########################################################################################
	# Step 4 - Clean Up
	##########################################################################################

	rm -rf CusDeploy
	rm -fr *${BUILD_LABEL}.zip
else
	echo "========================================================="
	echo " Image already exists ${DOCKER_REG}/${DOCKER_NAMESPACE}/${BUILD_TYPE}:${BUILD_LABEL}"
	echo "========================================================="
	echo
fi
